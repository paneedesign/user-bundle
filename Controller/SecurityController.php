<?php
/**
 * Created by PhpStorm.
 * User: fabianoroberto
 * Date: 20/08/15
 * Time: 12:37
 */

namespace PaneeDesign\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;

class SecurityController extends BaseSecurityController
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * {@inheritdoc}
     */
    public function loginAction(Request $request)
    {
        /** @var $session Session */
        $session = $request->getSession();

        $authErrorKey    = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);
        $csrfToken    = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        $requestAttributes = $request->attributes;

        if ($requestAttributes->get('_route') == 'fos_admin_user_security_login') {
            $template = $this->getParameter('ped_user.admin_login.template');
        } else if ($requestAttributes->get('_route') == 'fos_third_user_security_login') {
            $template = $this->getParameter('ped_user.third_login.template');
        } else if ($requestAttributes->get('_route') == 'fos_fourth_user_security_login') {
            $template = $this->getParameter('ped_user.fourth_login.template');
        } else {
            $template = $this->getParameter('ped_user.login.template');
        }

        return $this->renderLogin([
            'last_username' => $lastUsername,
            'error'         => $error,
            'csrf_token'    => $csrfToken,
            'template'      => $template,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function renderLogin(array $data)
    {
        if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('ped_user_homepage');
        }

        return $this->render($data['template'], $data);
    }
}