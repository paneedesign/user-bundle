<?php

namespace PaneeDesign\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\Loader\LoaderInterface;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class PedUserExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('ped_user.login.template', $config['login']['template']);
        $container->setParameter('ped_user.admin_login.template', $config['admin_login']['template']);
        $container->setParameter('ped_user.third_login.template', $config['third_login']['template']);
        $container->setParameter('ped_user.fourth_login.template', $config['fourth_login']['template']);

        $container->setParameter(
            'ped_user.registration.template',
            $config['registration']['template']
        );
        $container->setParameter(
            'ped_user.registration.check_email.template',
            $config['registration']['check_email']['template']
        );
        $container->setParameter(
            'ped_user.registration.confirmed.template',
            $config['registration']['confirmed']['template']
        );

        $container->setParameter(
            'ped_user.third_registration.template',
            $config['third_registration']['template']
        );
        $container->setParameter(
            'ped_user.third_registration.check_email.template',
            $config['third_registration']['check_email']['template']
        );
        $container->setParameter(
            'ped_user.third_registration.confirmed.template',
            $config['third_registration']['confirmed']['template']
        );

        $container->setParameter(
            'ped_user.resetting.template',
            $config['resetting']['template']
        );
        $container->setParameter(
            'ped_user.resetting.check_email.template',
            $config['resetting']['check_email']['template']
        );
        $container->setParameter(
            'ped_user.resetting.reset.template',
            $config['resetting']['reset']['template']
        );

        $container->setParameter(
            'ped_user.third_resetting.template',
            $config['third_resetting']['template']
        );
        $container->setParameter(
            'ped_user.third_resetting.check_email.template',
            $config['third_resetting']['check_email']['template']
        );
        $container->setParameter(
            'ped_user.third_resetting.reset.template',
            $config['third_resetting']['reset']['template']
        );

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $configs = $container->getExtensionConfig($this->getAlias());
        $config  = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $this->prependHwiOauth($container, $loader);
    }

    /**
     * @param ContainerBuilder $container
     * @param LoaderInterface $loader
     */
    private function prependHwiOauth(ContainerBuilder $container, LoaderInterface $loader)
    {
        if (!$container->hasExtension('hwi_oauth')) {
            return;
        }

        $loader->load('../services/integrations/hwi_oauth.yml');
    }
}