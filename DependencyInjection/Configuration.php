<?php

namespace PaneeDesign\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ped_user');


        $rootNode
            ->children()
                ->arrayNode('login')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('template')->defaultValue('PedUserBundle:Security:login.html.twig')->end()
                    ->end()
                ->end()
                ->arrayNode('admin_login')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('template')->defaultValue('PedUserBundle:Security:login.html.twig')->end()
                    ->end()
                ->end()
                ->arrayNode('third_login')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('template')->defaultValue('PedUserBundle:Security:login.html.twig')->end()
                    ->end()
                ->end()
                ->arrayNode('fourth_login')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('template')->defaultValue('PedUserBundle:Security:login.html.twig')->end()
                    ->end()
                ->end()
                ->arrayNode('registration')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('template')->defaultValue('PedUserBundle:Registration:register.html.twig')->end()
                        ->arrayNode('check_email')
                            ->addDefaultsIfNotSet()
                            ->canBeUnset()
                            ->children()
                                ->scalarNode('template')->defaultValue('PedUserBundle:Registration:check_email.html.twig')->end()
                            ->end()
                        ->end()
                        ->arrayNode('confirmed')
                            ->addDefaultsIfNotSet()
                            ->canBeUnset()
                            ->children()
                                ->scalarNode('template')->defaultValue('PedUserBundle:Registration:confirmed.html.twig')->end()
                            ->end()
                            ->end()
                        ->end()
                ->end()
                ->arrayNode('third_registration')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('template')->defaultValue('PedUserBundle:Registration:register.html.twig')->end()
                        ->arrayNode('check_email')
                            ->addDefaultsIfNotSet()
                            ->canBeUnset()
                            ->children()
                                ->scalarNode('template')->defaultValue('PedUserBundle:Registration:check_email.html.twig')->end()
                            ->end()
                        ->end()
                        ->arrayNode('confirmed')
                            ->addDefaultsIfNotSet()
                            ->canBeUnset()
                            ->children()
                                ->scalarNode('template')->defaultValue('PedUserBundle:Registration:confirmed.html.twig')->end()
                            ->end()
                            ->end()
                        ->end()
                ->end()
                ->arrayNode('resetting')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('template')->defaultValue('PedUserBundle:Resetting:request.html.twig')->end()
                        ->arrayNode('check_email')
                            ->addDefaultsIfNotSet()
                            ->canBeUnset()
                            ->children()
                                ->scalarNode('template')->defaultValue('PedUserBundle:Resetting:check_email.html.twig')->end()
                            ->end()
                        ->end()
                        ->arrayNode('reset')
                            ->addDefaultsIfNotSet()
                            ->canBeUnset()
                            ->children()
                                ->scalarNode('template')->defaultValue('PedUserBundle:Resetting:reset.html.twig')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('third_resetting')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('template')->defaultValue('PedUserBundle:Resetting:request.html.twig')->end()
                        ->arrayNode('check_email')
                            ->addDefaultsIfNotSet()
                            ->canBeUnset()
                            ->children()
                                ->scalarNode('template')->defaultValue('PedUserBundle:Resetting:check_email.html.twig')->end()
                            ->end()
                        ->end()
                        ->arrayNode('reset')
                            ->addDefaultsIfNotSet()
                            ->canBeUnset()
                            ->children()
                                ->scalarNode('template')->defaultValue('PedUserBundle:Resetting:reset.html.twig')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
