<?php

namespace PaneeDesign\UserBundle\Form\Extension;

use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormTypeExtension extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Adding new fields works just like in the parent form type.
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class);
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return RegistrationFormType::class;
    }
}
