Pane e Design - User Bundle
===========================

Users management for Symfony3 projects.

Installation
============

Step 1: Download the Bundle
---------------------------

Pane&Design repository is private so, add to `composer.json` this `vcs`

```json
    "repositories" : [
        ...
        {
            "type" : "vcs",
            "url" : "git@bitbucket.org:paneedesign/user-bundle.git"
        }
    ],
    ...
    "require": {
        ...
        "paneedesign/user-bundle": "^1.0"   
    }
```

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer update "paneedesign/user-bundle"
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Step 2: Enable the Bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...

            new \FOS\UserBundle\FOSUserBundle(),
            new \PaneeDesign\DiscriminatorMapBundle\PedDiscriminatorMapBundle(),
            new \PUGX\MultiUserBundle\PUGXMultiUserBundle(),
            new \PaneeDesign\UserBundle\PedUserBundle(),
        );

        // ...
    }

    // ...
}
```

Step 3: Configurations
----------------------

Add configuration

```yml
// app/config/config.yml
//...
imports:
    - { resource: "@PedUserBundle/Resources/config/config.yml" }
...
  
ped_user:
    login:
        template:     AppBundle:pages/customer:login.html.twig
    admin_login:
        template:     AppBundle:pages/admin:login.html.twig
    third_login:
        template:     AppBundle:pages/company:login.html.twig

    registration:
        template:     AppBundle:pages/customer:signup.html.twig
        check_email:
            template: AppBundle:pages/customer:onboarding.html.twig
        confirmed:
            template: AppBundle:pages/customer:onboarding.html.twig

    third_registration:
        template:     AppBundle:pages/company:signup.html.twig
        check_email:
            template: AppBundle:pages/company:onboarding.html.twig
        confirmed:
            template: AppBundle:pages/company:onboarding.html.twig

    resetting:
        template:     AppBundle:pages/customer:reset.html.twig
        check_email:
            template: AppBundle:pages/customer:resetting/checkEmail.html.twig
        reset:
            template: AppBundle:pages/customer:resetting/reset.html.twig
  
pugx_multi_user:
    users:
        customer:
            entity:
                class: AppBundle\Entity\Customer
            registration:
                form:
                    type: AppBundle\Form\Customer\RegistrationType
                    name: app_customer_registration
        admin:
            entity:
                class: AppBundle\Entity\Admin
            registration:
                form:
                    type: AppBundle\Form\Admin\RegistrationType
                    name: app_admin_registration
        company:
            entity:
                class: AppBundle\Entity\Company
            registration:
                form:
                    type: AppBundle\Form\Company\RegistrationType
                    name: app_company_registration
        ...
  
ped_discriminator_map:
    maps:
        user:
            entity: PaneeDesign\UserBundle\Entity\User
            children:
                customer: AppBundle\Entity\Customer
                admin:    AppBundle\Entity\Admin
                company:  AppBundle\Entity\Company
                ...
```

```yml
// app/config/routing.yml
ped_user:
    resource: '@PedUserBundle/Resources/config/routing.yml'
    prefix:   /
...
```