<?php

namespace PaneeDesign\UserBundle\OAuth;

use HWI\Bundle\OAuthBundle\Connect\AccountConnectorInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseUserProvider;

use PaneeDesign\UserBundle\Entity\Manager\UserManagerInterface;
use PaneeDesign\UserBundle\Entity\User;
use PaneeDesign\UserBundle\Entity\UserOAuth;

use Symfony\Component\Security\Core\User\UserInterface;

class UserProvider extends BaseUserProvider implements AccountConnectorInterface, OAuthAwareUserProviderInterface
{
    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var array
     */
    protected $properties = array(
        'identifier' => 'id',
    );

    /**
     * {@inheritdoc}
     */
    public function __construct(UserManagerInterface $userManager, array $properties)
    {
        parent::__construct($userManager, $properties);

        $this->userManager = $userManager;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $oauth = $this->userManager->findUserByProviderAndIdentifier(
            $response->getResourceOwner()->getName(),
            $response->getUsername()
        );

        if ($oauth instanceof UserOAuth) {
            return $oauth->getUser();
        }

        if (null !== $response->getEmail()) {
            $user = $this->userManager->findUserByEmail($response->getEmail());
            if (null !== $user) {
                return $this->updateUserByOAuthUserResponse($user, $response);
            }
        }

        return $this->createUserByOAuthUserResponse($response);
    }

    /**
     * {@inheritdoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        /* @var $user User */
        $this->updateUserByOAuthUserResponse($user, $response);
    }

    /**
     * Ad-hoc creation of user.
     *
     * @param UserResponseInterface $response
     *
     * @return User
     */
    private function createUserByOAuthUserResponse(UserResponseInterface $response)
    {
        /** @var User $user */
        $user = $this->userManager->createUser();

        $username  = $response->getUsername();
        $firstName = '';
        $lastName  = '';

        // set default values taken from OAuth sign-in provider account
        if(null !== $response->getEmail()) {
            $email = $response->getEmail();
        } else {
            $email = $username . '@facebook.com';
        }

        if (null !== $response->getFirstName()) {
            $firstName = $response->getFirstName();
            $lastName  = $response->getLastName();
        } else if (null !== $realName = $response->getRealName()) {
            $name      = explode(' ', $realName);
            $firstName = array_shift($name);
            $lastName  = implode(' ', $name);
        }

        $user->setEmail($email);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setUsername($username);

        // set random password to prevent issue with not nullable field & potential security hole
        $user->setPlainPassword(substr(sha1($response->getAccessToken()), 0, 10));
        $user->setEnabled(true);

        return $this->updateUserByOAuthUserResponse($user, $response);
    }

    /**
     * Attach OAuth sign-in provider account to existing user.
     *
     * @param UserInterface         $user
     * @param UserResponseInterface $response
     *
     * @return User
     */
    private function updateUserByOAuthUserResponse(UserInterface $user, UserResponseInterface $response)
    {
        $oauth = new UserOAuth();
        $oauth->setIdentifier($response->getUsername());
        $oauth->setProvider($response->getResourceOwner()->getName());
        $oauth->setAccessToken($response->getAccessToken());
        $oauth->setRefreshToken($response->getRefreshToken());

        /* @var $user User */
        $user->addOAuthAccount($oauth);

        $this->userManager->persist($user);
        $this->userManager->flush();

        return $user;
    }
}
