<?php
/**
 * Created by PhpStorm.
 * User: fabianoroberto
 * Date: 09/06/17
 * Time: 10:05
 */

namespace PaneeDesign\UserBundle\Entity\Manager;

use FOS\UserBundle\Model\UserManagerInterface as BaseUserManagerInterface;

interface UserManagerInterface extends BaseUserManagerInterface
{
    public function findUserByProviderAndIdentifier($provider, $identifier);

    public function persist($class);

    public function flush();
}