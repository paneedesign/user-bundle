<?php

namespace PaneeDesign\UserBundle\Entity\Manager;

use PUGX\MultiUserBundle\Doctrine\UserManager as BaseManager;

class UserManager extends BaseManager implements UserManagerInterface
{
    public function setUserDiscriminatorClass($class)
    {
        $this->userDiscriminator->setClass($class);
    }

    public function findUserByProviderAndIdentifier($provider, $identifier)
    {
        $repo = $this->om->getRepository('PedUserBundle:UserOAuth');

        return $repo->findOneBy(
            [
                'provider' => $provider,
                'identifier' => $identifier,
            ]
        );
    }

    public function persist($class)
    {
        $this->om->persist($class);
    }

    public function flush()
    {
        $this->om->flush();
    }
}