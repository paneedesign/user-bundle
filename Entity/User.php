<?php

namespace PaneeDesign\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PaneeDesign\UserBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="user_grant", type="string")
 * @ORM\DiscriminatorMap({"user" = "User"})
 */
abstract class User extends BaseUser
{
    const USER = 'user';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * Firstname of user
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Please enter your firstname.", groups={"User"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The firstname is too short.",
     *     maxMessage="The firstname is too long.",
     *     groups={"User"}
     * )
     *
     */
    protected $firstName;

    /**
     * Lastname of user
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Please enter your lastname.", groups={"User"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The lastname is too short.",
     *     maxMessage="The lastname is too long.",
     *     groups={"User"}
     * )
     *
     */
    protected $lastName;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="last_update_on", type="datetime")
     */
    protected $lastUpdateOn;

    /**
     * @ORM\OneToMany(targetEntity="PaneeDesign\UserBundle\Entity\UserOAuth", mappedBy="user", cascade={"all"})
     **/
    protected $oauthAccounts;

    public function __construct()
    {
        parent::__construct();

        $this->oauthAccounts = new ArrayCollection();
    }

    /**
     *  @ORM\PrePersist()
     */
    public function doStuffOnPrePersist()
    {
        $this->createdAt    = new \DateTime("now");
        $this->lastUpdateOn = new \DateTime("now");
    }

    /**
     *  @ORM\PreUpdate()
     */
    public function doStuffOnPreUpdate()
    {
        $this->lastUpdateOn = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the email.
     *
     * @param string $email
     * @param bool $setUsername
     *
     * @return BaseUser
     */
    public function setEmail($email, $setUsername = true)
    {
        if($setUsername) {
            $this->setUsername($email);
        }

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @param bool $setUsernameCanonical
     * @return BaseUser
     */
    public function setEmailCanonical($emailCanonical, $setUsernameCanonical = true)
    {
        if($setUsernameCanonical) {
            $this->setUsernameCanonical($emailCanonical);
        }

        return parent::setEmailCanonical($emailCanonical);
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set lastUpdateOn
     *
     * @param \DateTime $lastUpdateOn
     *
     * @return User
     */
    public function setLastUpdateOn($lastUpdateOn)
    {
        $this->lastUpdateOn = $lastUpdateOn;

        return $this;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Get lastUpdateOn
     *
     * @return \DateTime
     */
    public function getLastUpdateOn()
    {
        return $this->lastUpdateOn;
    }

    /**
     * Add oauthAccount
     *
     * @param UserOAuth $oauthAccount
     *
     * @return User
     */
    public function addOauthAccount(UserOAuth $oauthAccount)
    {
        if (!$this->oauthAccounts->contains($oauthAccount)) {
            $this->oauthAccounts->add($oauthAccount);
            $oauthAccount->setUser($this);
        }

        return $this;
    }

    /**
     * Remove oauthAccount
     *
     * @param UserOAuth $oauthAccount
     */
    public function removeOauthAccount(UserOAuth $oauthAccount)
    {
        $this->oauthAccounts->removeElement($oauthAccount);
    }

    /**
     * Get oauthAccounts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOauthAccounts()
    {
        return $this->oauthAccounts;
    }

    /**
     * Get user grant
     *
     * @return string
     */
    public function getUserGrant()
    {
        return self::USER;
    }
}
